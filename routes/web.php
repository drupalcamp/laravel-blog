<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Blog;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
  return view('test');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/blogs', function() {
  // $listBlogs = App\Models\Blog::all(); // It is another way to call;
  $listBlogs = Blog::all();

  // $listBlogs = [
  //   [
  //     'title' => "Ttitle 1",
  //     'body' => "Body 1",
  //     'url' => 'https//youtube.com',
  //   ],
  //   [
  //     'title' => "Ttitle 2",
  //     'body' => "Body 2",
  //     'url' => 'https//youtube.com/1',
  //   ]   
  // ];

  return view('blogs', ['blogs' => $listBlogs] );

});

// Route::get('/submit', function () {
//   return view('submit');
// });



Route::get('/submit', [App\Http\Controllers\SubmitController::class, 'index'])->name('submit');


Route::post('/submit', function (Request $request) {

  $blogData = $request->validate(
    [
      'title' => 'required|max:255',
      'url' =>  'required|url|max:255',
      'body' =>  'required|max:255',
    ]
  );

  // $blog = tap(new Blog($blogData))->save();

  $blog = new Blog;
  $blog->title = $blogData['title'];
  $blog->url = $blogData['url'];
  $blog->body = $blogData['body'];
  $blog->save();

  return redirect('/blogs');
});