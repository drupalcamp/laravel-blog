<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    // public function all() {
    //   return [
    //     [
    //     'title' => "Ttitle 1",
    //     'body' => "Body 1",
    //     'url' => 'https//youtube.com',
    //     ]
    //   ];
    // }
    protected $fillable = [
      'title',
      'url',
      'description'
  ];
}
