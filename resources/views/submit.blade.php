@extends('layouts.app')

@section('content')

<div class="row">
  <h1>Submit your post</h1>
</div>
<div class="row">
  <form action="/submit" class="submitform" method="post">
  @csrf

  @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        Please fix the following errors
    </div>
  @endif

  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" placeholder="Title" value="{{ old('title') }}">
    @error('title')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror    
  </div>

  <div class="form-group">
    <label for="url">Url</label>
    <input type="text" class="form-control @error('url') is-invalid @enderror" id="url" name="url" placeholder="URL" value="{{ old('url') }}">
    @error('url')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control @error('body') is-invalid @enderror" id="body" name="body" placeholder="Your Post">{{ old('body') }}</textarea>
    @error('body')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>


  </form>
</div>

@endsection